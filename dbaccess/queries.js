var promise = require('bluebird');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://postgres:Gn1357Zx@localhost:5432/zxheroes';
var db = pgp(connectionString);

// add query functions

module.exports = {
  getAllHeroes: getAllHeroes,
  getAllTasks: getAllTasks,
  getSingleHero: getSingleHero,
  getSingleTask: getSingleTask,
  createHero: createHero,
  createTask: createTask,
  updateHero: updateHero,
  updateTask: updateTask,
  removeHero: removeHero,
  removeTask: removeTask
};

function getAllHeroes(req, res, next){
  db.any('select * from heroes') // any, one, many, none, result - očekujemo li određeni broj?
    .then(function(data){
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved all heroes'
        });
    })
    .catch(function(err){
      console.log(err);
      return next(err);
    });
};

function getAllTasks(req, res, next){
  db.any('select * from tasks') // any, one, many, none, result - očekujemo li određeni broj?
    .then(function(data){
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved all tasks'
        });
    })
    .catch(function(err){
      console.log(err);
      return next(err);
    });
};


function getSingleHero(req, res, next){
  var heroId = req.params.id;
  db.one('select * from heroes where ID = $1', heroId) // any, one, many, none, result - očekujemo li određeni broj?
    .then(function(data){
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved single hero'
        });
    })
    .catch(function(err){
      console.log(err);
      return next(err);
    });
};

function getSingleTask(req, res, next){
  var taskId = req.params.id;
  db.one('select * from tasks where ID = $1', taskId) // any, one, many, none, result - očekujemo li određeni broj?
    .then(function(data){
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved single task'
        });
    })
    .catch(function(err){
      console.log(err);
      return next(err);
    });
};

function createHero(req, res, next) {
  req.body.experience = parseInt(req.body.experience);
  req.body.points = parseInt(req.body.points);
  db.none('insert into heroes(name, role, experience, points)' +
      'values(${name}, ${role}, ${experience}, ${points})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one hero'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
};

function createTask(req, res, next) {
  req.body.heroID = parseInt(req.body.heroID);
  req.body.value = parseInt(req.body.value);
  db.none('insert into tasks(heroID, name, datedue, value)' +
      'values($(heroID), ${name}, ${datedue}, ${value})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one task'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
};


function updateHero(req, res, next) {
  db.none('update heroes set name=$1, role=$2, experience=$3, points=$4 where id=$5',
    [req.body.name, req.body.role, parseInt(req.body.experience),
      parseInt(req.body.points), parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated hero'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
};

function updateTask(req, res, next) {
  db.none('update tasks set heroID = $1, name=$2, datedue=$3, value=$4 where id=$5',
    [parseInt(req.body.heroID), req.body.name, req.body.datedue, parseInt(req.body.value),
       parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated task'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
};

function removeHero(req, res, next) {
  var heroID = parseInt(req.params.id);
  db.result('delete from heroes where id = $1', heroID)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} hero(es)`
        });
    })
    .catch(function (err) {
      return next(err);
    });
};

function removeTask(req, res, next) {
  var taskID = parseInt(req.params.id);
  db.result('delete from tasks where id = $1', taskID)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} task(s)`
        });
    })
    .catch(function (err) {
      return next(err);
    });
};