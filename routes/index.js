var express = require('express');
var router = express.Router();

var db = require('../dbaccess/queries');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Zeraxo Heroes Task Manager' });
});

/* GET all heroes listed */
router.get('/api/heroes', db.getAllHeroes);

/* GET all tasks listed */
router.get('/api/tasks', db.getAllTasks);

/* GET single hero by ID */
router.get('/api/heroes/:id', db.getSingleHero);

/* GET single task by ID */
router.get('/api/tasks/:id', db.getSingleTask);

/* POST new hero */
router.post('/api/heroes', db.createHero);

/* POST new task */
router.post('/api/tasks', db.createTask);

/* PUT update hero by ID */
router.put('/api/heroes/:id', db.updateHero);

/* PUT update task by ID */
router.put('/api/tasks/:id', db.updateTask);

/* DELETE hero by ID */
router.delete('/api/heroes/:id', db.removeHero);

/* DELETE task by ID */
router.delete('/api/tasks/:id', db.removeTask);

module.exports = router;
