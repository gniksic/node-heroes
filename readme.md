# NodeJS + express + PostgreSQL example API for developer and task management

Document version: 0.0.1

Last update: 2017-07-03T11:30+02:00

This document specifies the RESTful API for creating, reading, updating and deleting (CRUD) developers and tasks.

## File structure

All documents are located as follows

* Application root
	- routes
		- index.js - Main route definitions and reference to the database actions
    - routes
        - queries.js - database connection and CRUD commands
    - public
        - images, scripts and css
    - views
    	- Jade templates for the home and error pages
    - app.js - main app script
    - package.json - npm package dependencies
    - zxheroes.sql - PostgreSQL initialisation
    - readme.md - this document

## Purpose and use cases

This is an example API using elementary HTTP requests (POST, GET, PUT, DELETE) to perform CRUD operations on developers and tasks, linked with a foreign key, so that a task always belongs to someone.

The purpose is to demonstrate the ease of creating RESTful APIs with minimal coding using NodeJS, Express web server for Node and a PostgreSQL database.

This example is supposed to be used together with ARC or Postman applications for Chrome in order to perform requests and test all functionality, without a frontend application.