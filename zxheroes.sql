DROP DATABASE IF EXISTS zxheroes;
CREATE DATABASE zxheroes;

\c zxheroes;

CREATE TABLE heroes (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  role VARCHAR,
  experience INTEGER,
  points INTEGER
);

/* Unicode name, job role, exp. in months, goal points */
INSERT INTO heroes (name, role, experience, points)
  VALUES ('Goran', 'developer', 4, 10);


CREATE TABLE tasks (
  ID SERIAL PRIMARY KEY,
  heroID INTEGER REFERENCES heroes,
  name VARCHAR,
  datedue TIMESTAMP,
  value INTEGER
);

/* Foreign key ID, Unicode name, task name, datetime due, task value points */
INSERT INTO tasks (heroID, name, datedue, value)
  VALUES (1, 'ePorezna', '2017-07-10T10:00:00' , 100);
